import {
  noLoadingRequest,
  loadingRequest
} from "../utils/request"

// 分页查询诉求
/**
 * @param {String} openid // 需要查询谁的诉求 不传就查询全部
 * @param {String} title //模糊查询 标题
 * @param {Boolean} createTime //根据时间排序
 * @param {Boolean} browseCount //根据浏览量排序
 * @param {Boolean} nearby // 是否根据附近
 * @param {} longitude // 经度
 * @param {} latitude // 纬度
 * @param {Number} pageNO //页数
 * @param {Number} pageSize //每页数据量
 */
export const pageAppeal = async function (params) {
  let data = {
    api: 'pageAppeal',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'appeal'
  })
  return result.data
}

// 分页查询诉求评论
/**
 * @param {String} appealId //诉求id
 * @param {Number} pageNO //页数
 * @param {Number} pageSize //每页数据量
 */
export const pageAppealComment = async function (params) {
  let data = {
    api: 'pageAppealComment',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'appeal'
  })

  return result.data
}


// 诉求详情
/**
 * @param {String} appealId //诉求id
 */
export const appealDetail = async function (params) {
  let data = {
    api: 'appealDetail',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'appeal'
  })
  return result.data
}


// 添加诉求
/**
 * @param {String} title //诉求标题
 * @param {String} content //诉求内容
 * @param {Arrray} appealTag //诉求标签
 * @param {Arrray} appealMaterial //诉求素材
 * @param {geopoint} latitude //坐标 纬度
 * @param {geopoint} longitude //坐标 经度
 */
export const appealSave = async function (params) {
  let data = {
    api: 'appealSave',
    data: params
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'appeal'
  })

  if (result.code != '00000') {
    wx.showToast({
      title: '发送失败',
      icon: 'none'
    })
    return false
  }

  return result.data
}

// 诉求点赞
/**
 * @param {String} appealId //诉求id
 */
export const appealToEndorse = async function (params) {

  const header = {
    name: 'appeal',
    api: 'appealToEndorse',
    isUser: true
  }

  const result = await noLoadingRequest(header, params)

  return result.data
}

// 保存诉求评论
/**
 * @param {String} appealId //诉求id
 * @param {String} content // 评论内容
 */
export const appealPutComment = async function (params) {

  const header = {
    name: 'appeal',
    api: 'appealPutComment',
    isUser: true
  }

  const result = await loadingRequest(header, params)

  console.log(result)

  return result.data

}

// 取消 诉求点赞
/**
 * @param {String} appealId //诉求id
 */
export const appealToCancel = async function (params) {

  const header = {
    name: 'appeal',
    api: 'appealToCancel',
    isUser: true
  }

  const result = await noLoadingRequest(header, params)

  console.log(result)

  return result.data

}


// 诉求浏览记录
/**
 * @param {String} appealId //诉求id
 */
export const appealBrowseLog = async function (params) {
  let data = {
    api: 'appealBrowseLog',
    data: params,
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'appeal'
  })

  return result.data
}


// 查询所有诉求的标签
export const appealTag = async function (params) {
  let data = {
    api: 'appealTag',
    data: params,
  }
  const {
    result
  } = await wx.cloud.callFunction({
    data,
    name: 'appeal'
  })

  return result.data
}

