//动态评论接口
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database() //数据库

const dynamicComment = async function(params){
  const wxContext = cloud.getWXContext()
  const commentResult = await db.collection("dynamicComment").add({
    data:{
      _openid:wxContext.OPENID,
      createTime:new Date().getTime(),
      images:[],
      message:params.message,
      dynamicId:params.dynamicId
    }
  })
  return commentResult
}

module.exports = dynamicComment 